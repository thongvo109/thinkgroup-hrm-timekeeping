import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/screens/home.dart';

class Routes {
  static Map<String, WidgetBuilder> data = {
    '/': (context) => const HomePage(),
  };
}
