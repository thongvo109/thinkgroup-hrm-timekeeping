class ApiError {
  final bool? success;
  final int? code;
  final String? message;
  final List<String>? errors;
  final String json;
  ApiError(this.success, this.code, this.message, this.errors, this.json);
}
