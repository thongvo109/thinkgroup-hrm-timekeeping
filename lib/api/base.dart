import 'dart:io' show Platform;

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';
import 'package:path/path.dart' as p;
import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/api/log/logger_interceptor.dart';

class BaseApi extends Interceptor {
  late Dio dio;

  BaseApi() {
    //Setup the option for dia
    BaseOptions options = BaseOptions(
      baseUrl: 'https://api-staging.thinkpro.vn/hrm/app/v2/',
      receiveTimeout: 15000,
      connectTimeout: 20000,
    );

    dio = Dio(options);

    if (kDebugMode) {
      dio.interceptors.add(LoggerInterceptor());
    }

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options,
            RequestInterceptorHandler requestInterceptorHandler) async {
          if (options.data != null && options.data is FormData) {
            final FormData data = options.data as FormData;
            final files = data.files;

            for (int i = 0; i < files.length; i++) {
              String? path = files[i].value.filename;

              if (path == null) continue;

              if (path.toLowerCase().contains('.heic')) {
                final pathJpg = await HeicToJpg.convert(path);
                final name = p.basename(pathJpg!);
                files[i] = MapEntry(files[i].key,
                    MultipartFile.fromFileSync(path, filename: name));
              } else {
                final name = p.basename(path);
                files[i] = MapEntry(files[i].key,
                    MultipartFile.fromFileSync(path, filename: name));
              }
            }
          }
          return requestInterceptorHandler.next(options);
        },
        onError: onError,
      ),
    );
  }

  @override
  Future onError(DioError err, ErrorInterceptorHandler handler) {
    super.onError(err, handler);
    return Future.value(err);
  }

  static String getMappedPlatformName() {
    String name = 'Unknown';

    if (Platform.isAndroid) {
      name = 'Android';
    } else if (Platform.isIOS) {
      name = 'iOS';
    } else if (Platform.isWindows) {
      name = 'Windows';
    } else if (Platform.isLinux) {
      name = 'Linux';
    } else if (Platform.isMacOS) {
      name = 'MacOS';
    } else if (Platform.isFuchsia) {
      name = 'Fuchsia';
    }

    return name;
  }

  ApiError handleError(dynamic error) {
    bool success = false;
    int code = 0;
    String message = 'Đã có lỗi';
    List<String> errors = [];
    String json = '';
    if (error is DioError) {
      switch (error.type) {
        case DioErrorType.sendTimeout:
          message = 'api_error_send_timeout';
          break;
        case DioErrorType.cancel:
          message = 'api_error_cancel';
          break;
        case DioErrorType.connectTimeout:
          message = 'api_error_connect_timeout';
          break;
        case DioErrorType.other:
          message = 'api_error_default';
          break;
        case DioErrorType.receiveTimeout:
          message = 'api_error_receive_timeout';
          break;
        case DioErrorType.response:
          debugPrint('handleError: ${error.response}');
          success = error.response!.data['error']['success'] ?? false;
          code = error.response!.data['error']['code'] ?? 0;
          message = error.response!.data['error']['message'] ?? '';
          errors = error.response!.data['error']['erros'] ?? [];
          json = error.response!.data == null
              ? ''
              : error.response!.data.toString();
          break;
      }
    } else if (error is NoSuchMethodError) {
      json = error.toString();
      message = 'Lỗi kiểu dữ liệu.';
    } else {
      message = 'api_error_unexpected $error';
    }

    return ApiError(success, code, message, errors, json);
  }
}
