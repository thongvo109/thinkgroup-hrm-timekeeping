import 'package:dio/dio.dart';
import 'package:thinkgroup_timekeeping_app/api/base.dart';
import 'package:thinkgroup_timekeeping_app/collections/staffs_collection.dart';
import 'package:thinkgroup_timekeeping_app/collections/timekeeping_response.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_detail_model.dart';

class StaffsApi extends BaseApi {
  final String _endpoint = 'timekeeping/staffs';
  final String _checkEndPoint = 'timekeeping';

  Future<StaffsCollection?> getListStaff() async {
    try {
      Response response = await dio.get(_endpoint);
      return StaffsCollection.fromJson(response.data);
    } catch (e) {
      return StaffsCollection.withError(handleError(e));
    }
  }

  Future<StaffDetailModel?> getDetail(int id) async {
    try {
      Response response = await dio.get(_endpoint + "/info" + "/$id");

      return StaffDetailModel.fromJson(response.data['data']);
    } catch (e) {
      return StaffDetailModel.withError(handleError(e));
    }
  }

  Future<TimeKeepingResponse> check(
      {required FormData formData, required bool checkIn}) async {
    String path = checkIn ? '/checkin' : '/checkout';
    try {
      Response response = await dio.post(_checkEndPoint + path, data: formData);
      return TimeKeepingResponse.fromJson(response.data);
    } catch (e) {
      return TimeKeepingResponse.withError(handleError(e));
    }
  }
}
