import 'package:flutter/material.dart';

import 'app_color.dart';

ThemeData get getTheme => ThemeData(
      primarySwatch: AppColor.primary,
      brightness: Brightness.light,
      backgroundColor: AppColor.white,
      scaffoldBackgroundColor: AppColor.white,
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          minimumSize: const Size(10, 48),
          padding: const EdgeInsets.symmetric(
            vertical: 16,
            horizontal: 8,
          ),
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        border: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xFF000000), width: 0.1),
          borderRadius: BorderRadius.circular(4),
        ),
        disabledBorder: OutlineInputBorder(
          borderSide: const BorderSide(color: Color(0xFF000000), width: 0.1),
          borderRadius: BorderRadius.circular(4),
        ),
      ),
    );
