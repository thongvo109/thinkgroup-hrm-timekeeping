// import 'package:flutter/material.dart';
// import 'package:flutter_svg/flutter_svg.dart';

// class AppDrawable {
//   static const iconPath = 'assets/icons';
//   static const imagePath = 'assets/images';
//   static final _icMenuCalendarActivate =
//       SvgPicture.asset('$iconPath/ic_menu_calendar_activate.svg');
//   static final _icMenuCalendarDeactivate =
//       SvgPicture.asset('$iconPath/ic_menu_calendar_deactivate.svg');
//   static final _icMenuHomeActivate =
//       SvgPicture.asset('$iconPath/ic_menu_home_activate.svg');
//   static final _icMenuHomeDeactivate =
//       SvgPicture.asset('$iconPath/ic_menu_home_deactivate.svg');
//   static final _icMenuKpiActivate =
//       SvgPicture.asset('$iconPath/ic_menu_kpi_activate.svg');
//   static final _icMenuKpiDeactivate =
//       SvgPicture.asset('$iconPath/ic_menu_kpi_deactivate.svg');
//   static final _icMenuProfileActivate =
//       SvgPicture.asset('$iconPath/ic_menu_profile_activate.svg');
//   static final _icMenuProfileDeactivate =
//       SvgPicture.asset('$iconPath/ic_menu_profile_deactivate.svg');
//   static final icNotify = SvgPicture.asset('$iconPath/ic_notify.svg');
//   static final icCalendar = SvgPicture.asset('$iconPath/ic_calendar.svg');
//   static final icCalendarEdit =
//       SvgPicture.asset('$iconPath/ic_calendar_edit.svg');
//   static final icCalendarStaff =
//       SvgPicture.asset('$iconPath/ic_calendar_staff.svg');
//   static final icTransferToCompany =
//       SvgPicture.asset('$iconPath/ic_transfer_to_company.svg');
//   static final icSalaryBonus =
//       SvgPicture.asset('$iconPath/ic_salary_bonus.svg');
//   static final icInvoiceParticipant =
//       SvgPicture.asset('$iconPath/ic_invoice_participant.svg');
//   static final icCheckInventory =
//       SvgPicture.asset('$iconPath/ic_check_inventory.svg');
//   static final imgBlueActionBg =
//       Image.asset('$imagePath/img_blue_action_bg.png', fit: BoxFit.fill);
//   static final imgGreenActionBg =
//       Image.asset('$imagePath/img_green_action_bg.png', fit: BoxFit.fill);
//   static final imgVioletActionBg =
//       Image.asset('$imagePath/img_violet_action_bg.png', fit: BoxFit.fill);
//   static final imgOrangeActionBg =
//       Image.asset('$imagePath/img_orange_action_bg.png', fit: BoxFit.fill);
//   static final icPlus = SvgPicture.asset('$iconPath/ic_plus.svg');
//   static final icMinus = SvgPicture.asset('$iconPath/ic_minus.svg');
//   static final icNotificationSettings =
//       SvgPicture.asset('$iconPath/ic_notification_setting.svg');
//   static final icNotifyBonus =
//       SvgPicture.asset('$iconPath/ic_notify_bonus.svg');
//   static final icNotifyDiscipline =
//       SvgPicture.asset('$iconPath/ic_notify_discipline.svg');
//   static final icNotifyOfferSchedule =
//       SvgPicture.asset('$iconPath/ic_notify_offer_schedule.svg');
//   static final icNotifyOfferGoOn =
//       SvgPicture.asset('$iconPath/ic_notify_offer_go_on.svg');
//   static final imgLogo = Image.asset('$imagePath/img_logo.png');
//   static final icProfile = SvgPicture.asset('$iconPath/ic_profile.svg');
//   static final icEarnings = SvgPicture.asset('$iconPath/ic_earnings.svg');
//   static final icEarningsBranch =
//       SvgPicture.asset('$iconPath/ic_earnings_branch.svg');
//   static final icPayoff = SvgPicture.asset('$iconPath/ic_payoff.svg');
//   static final icInspect = SvgPicture.asset('$iconPath/ic_inspect.svg');
//   static final icChangeSchedule =
//       SvgPicture.asset('$iconPath/ic_change_schedule.svg');
//   static final icSettings = SvgPicture.asset('$iconPath/ic_settings.svg');
//   static final icLogout = SvgPicture.asset('$iconPath/ic_logout.svg');

//   static final _icNotifyGoOnRight =
//       SvgPicture.asset('$iconPath/ic_notify_go_on_right.svg');
//   static final _icNotifyGoOnWrong =
//       SvgPicture.asset('$iconPath/ic_notify_go_on_wrong.svg');
//   static final _icNotifyOctagonRight =
//       SvgPicture.asset('$iconPath/ic_notify_octagon_right.svg');
//   static final _icNotifyOctagonWrong =
//       SvgPicture.asset('$iconPath/ic_notify_octagon_wrong.svg');
//   static final _icNotifySalaryRight =
//       SvgPicture.asset('$iconPath/ic_notify_salary_right.svg');
//   static final _icNotifySalaryWrong =
//       SvgPicture.asset('$iconPath/ic_notify_salary_wrong.svg');
//   static final _icNotifyScheduleRight =
//       SvgPicture.asset('$iconPath/ic_notify_schedule_right.svg');
//   static final _icNotifyScheduleWrong =
//       SvgPicture.asset('$iconPath/ic_notify_schedule_wrong.svg');

//   static final icInvoiceParticipantDelivery =
//       SvgPicture.asset('$iconPath/ic_invoice_participant_delivery.svg');
//   static final icInvoiceParticipantHardware =
//       SvgPicture.asset('$iconPath/ic_invoice_participant_hardware.svg');
//   static final icInvoiceParticipantPackingShipping =
//       SvgPicture.asset('$iconPath/ic_invoice_participant_packing_shipping.svg');
//   static final icInvoiceParticipantWarrantyRepair =
//       SvgPicture.asset('$iconPath/ic_invoice_participant_warranty_repair.svg');

//   static final icRemove =
//       SvgPicture.asset('$iconPath/ic_remove.svg', width: 15, height: 15);
//   static final icAdd = SvgPicture.asset('$iconPath/ic_add.svg');
//   static final icBtnHeart = SvgPicture.asset('$iconPath/ic_btn_heart.svg');
//   static final icStart =
//       SvgPicture.asset('$iconPath/ic_start.svg', width: 20, height: 20);
//   static final icDissatisfied =
//       SvgPicture.asset('$iconPath/ic_dissatisfied.svg', width: 20, height: 20);

//   static SvgPicture icMenuCalendar(bool activate) =>
//       activate ? _icMenuCalendarActivate : _icMenuCalendarDeactivate;

//   static SvgPicture icMenuHome(bool activate) =>
//       activate ? _icMenuHomeActivate : _icMenuHomeDeactivate;

//   static SvgPicture icMenuKpi(bool activate) =>
//       activate ? _icMenuKpiActivate : _icMenuKpiDeactivate;

//   static SvgPicture icMenuProfile(bool activate) =>
//       activate ? _icMenuProfileActivate : _icMenuProfileDeactivate;

//   static SvgPicture icNotifyGoOn(bool right) =>
//       right ? _icNotifyGoOnRight : _icNotifyGoOnWrong;

//   static SvgPicture icNotifyOctagon(bool right) =>
//       right ? _icNotifyOctagonRight : _icNotifyOctagonWrong;

//   static SvgPicture icNotifySalary(bool right) =>
//       right ? _icNotifySalaryRight : _icNotifySalaryWrong;

//   static SvgPicture icNotifySchedule(bool right) =>
//       right ? _icNotifyScheduleRight : _icNotifyScheduleWrong;
// }
