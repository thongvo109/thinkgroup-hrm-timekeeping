import 'package:flutter/material.dart';

import 'app_color.dart';

final borderLine = OutlineInputBorder(
  borderRadius: BorderRadius.circular(4),
  borderSide: BorderSide(
    color: AppColor.black.withOpacity(0.12),
  ),
);

final borderFocusLine = OutlineInputBorder(
  borderRadius: BorderRadius.circular(4),
  borderSide: const BorderSide(
    color: AppColor.blueLight,
  ),
);