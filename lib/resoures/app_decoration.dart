import 'package:flutter/material.dart';

import 'app_color.dart';

class AppThemeDecoration {
  static final tabbarShadow = BoxDecoration(
    boxShadow: [
      BoxShadow(
        offset: const Offset(0, 1),
        color: AppColor.black.withOpacity(0.2),
        blurRadius: 8,
      ),
      BoxShadow(
        offset: const Offset(0, 3),
        color: AppColor.black.withOpacity(0.12),
        blurRadius: 3,
      ),
    ],
    color: AppColor.white,
  );
}
