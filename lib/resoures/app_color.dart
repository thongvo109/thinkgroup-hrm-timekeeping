import 'package:flutter/material.dart';

class AppColor {
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF000000);
  static const orange1 = Color(0xFFE9922C);
  static const orange2 = Color(0xFFFF8300);
  static const orangeLight = Color(0xFFFCEDE9);
  static const blue1 = Color(0xFF0046C1);
  static const blue2 = Color(0xFF1658DA);
  static const blue3 = Color(0xFF61A2EE);
  static const blue4 = Color(0xFFC6D3FF);
  static const neutral1 = Color(0xFF12006B);
  static const neutral2 = Color(0xFF666666);
  static const neutral3 = Color(0xFF9E9E9E);
  static const neutral5 = Color(0xFFC8C8C8);
  static const neutral6 = Color(0xFFF9F9F9);
  static const blueLight = Color(0xFF2758F2);
  static const gray1 = Color(0xFFE4E7EB);
  static const gray2 = Color(0xFFDADADA);
  static const gray3 = Color(0xFFE7E7E7);
  static const gray4 = Color(0xFFD0D0D0);
  static const grayDark = Color(0xFF979797);
  static const grayLight =Color(0xFFF5F5F5);
  static const green1 = Color(0xFF28C397);
  static const green2 = Color(0xFF3AB35A);
  static const greenLight1 = Color(0xFFEAF7EE);
  static const greenLight2 = Color(0xFF50ECC0);
  static const greenDark = Color(0xFF0F956F);
  static const red1 = Color(0xFFEE616A);
  static const red2 = Color(0xFFF15858);
  static const red3 = Color(0xFFF12734);
  static const redLight = Color(0xFFFFDEDE);
  static const surface = Color(0xFF212121);
  static const blueWhite = Color(0xFFEEF2FF);
  static const yellowLight = Color(0xFFFEF7EA);
  static const violet1 = Color(0xFF6F51FF);

  static const primary = MaterialColor(0xFF0046C1, {
    900: Color(0xFF001f97),
    800: Color(0xFF0032a9),
    700: Color(0xFF003db5),
    600: Color(0xFF0047c1),
    500: Color(0xFF0d4fcb),
    400: Color(0xFF4769d4),
    300: Color(0xFF6c85db),
    200: Color(0xFF98a7e5),
    100: Color(0xFFc2c9f0),
    50: Color(0xFFe7eaf9),
  });
}
