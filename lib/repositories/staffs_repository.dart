import 'package:dio/dio.dart';
import 'package:thinkgroup_timekeeping_app/api/staffs_api.dart';
import 'package:thinkgroup_timekeeping_app/collections/staffs_collection.dart';
import 'package:thinkgroup_timekeeping_app/collections/timekeeping_response.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_detail_model.dart';
import 'package:thinkgroup_timekeeping_app/repositories/base_repository.dart';

class StaffsReposioty extends BaseRepository {
  final StaffsApi _api = StaffsApi();

  Future<StaffsCollection?> loadList() async {
    return _api.getListStaff();
  }

  Future<StaffDetailModel?> loadDetail(int id) async {
    return _api.getDetail(id);
  }

  Future<TimeKeepingResponse> check(
      {required FormData formData, bool checkin = true}) async {
    return _api.check(formData: formData, checkIn: checkin);
  }
}
