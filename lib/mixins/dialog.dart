import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/widgets/error_dialog.dart';
import 'package:thinkgroup_timekeeping_app/widgets/indicator.dart';
import 'package:thinkgroup_timekeeping_app/widgets/loading_dialog.dart';
import 'package:thinkgroup_timekeeping_app/widgets/result_dialog.dart';
import 'package:thinkgroup_timekeeping_app/widgets/success_dialog.dart';

abstract class DialogMixin {
  void hideDialog(BuildContext context) {
    //Pops the progress dialog
    Navigator.pop(context);
  }

  void showProcessingDialog(
    BuildContext context, {
    String text = '',
    bool dismissible = false,
  }) async {
    if (text == '') {
      text = 'Đang xử lý';
    }

    return showDialog(
      barrierDismissible: dismissible,
      context: context,
      builder: (BuildContext context) {
        return LoadingDialog(text: text);
      },
    );
  }

  void showSuccessDialog(
    BuildContext context, {
    String text = '',
    bool dismissible = false,
  }) async {
    if (text == '') {
      text = 'Xử lý thành công';
    }

    return showDialog(
        barrierDismissible: dismissible,
        context: context,
        builder: (BuildContext context) {
          return SuccessDialog(text: text);
        });
  }

  void showErrorDialog(
    BuildContext context, {
    String text = '',
    bool dismissible = false,
  }) async {
    if (text == '') {
      text = 'Xử lý thành công';
    }

    return showDialog(
        barrierDismissible: dismissible,
        context: context,
        builder: (BuildContext context) {
          return ErrorDialog(text: text);
        });
  }

  void showDialogLoading(BuildContext context) async {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return const Indicator();
        });
  }

  void showDialogResult(
    BuildContext context, {
    String text = '',
    bool dismissible = false,
  }) async {
    if (text == '') {
      text = 'Xử lý thành công';
    }
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return ResultDialog(method: text);
        });
  }
}
