import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/resoures/app_theme.dart';
import 'package:thinkgroup_timekeeping_app/routers.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: getTheme,
      initialRoute: '/',
      routes: Routes.data,
    );
  }
}
