import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:jiffy/jiffy.dart';
import 'package:thinkgroup_timekeeping_app/contants.dart';
import 'package:thinkgroup_timekeeping_app/mixins/dialog.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_detail_model.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_model.dart';
import 'package:thinkgroup_timekeeping_app/repositories/staffs_repository.dart';
import 'package:thinkgroup_timekeeping_app/screens/camera_check.dart';
import 'package:thinkgroup_timekeeping_app/widgets/button.dart';
import 'package:thinkgroup_timekeeping_app/widgets/dialog.dart';
import 'package:thinkgroup_timekeeping_app/widgets/gallery_photo_view.dart';
import 'package:thinkgroup_timekeeping_app/widgets/indicator.dart';
import 'package:thinkgroup_timekeeping_app/widgets/lightbox_dialog.dart';
import 'package:thinkgroup_timekeeping_app/widgets/tag.dart';

class StaffDetailScreen extends StatefulWidget {
  final StaffsModel staffs;
  const StaffDetailScreen({Key? key, required this.staffs}) : super(key: key);

  @override
  State<StaffDetailScreen> createState() => _StaffDetailScreenState();
}

class _StaffDetailScreenState extends State<StaffDetailScreen>
    with AfterLayoutMixin<StaffDetailScreen>, DialogMixin {
  bool showError = false;
  bool isError = false;
  bool isLoading = true;
  StaffDetailModel? _staffDetail;
  @override
  void afterFirstLayout(BuildContext context) {
    loadDetail();
  }

  void loadDetail() async {
    if (widget.staffs.id != null) {
      StaffDetailModel? staffDetail =
          await StaffsReposioty().loadDetail(widget.staffs.id!);
      setState(() {
        if (staffDetail!.error == null) {
          _staffDetail = staffDetail;
          isLoading = false;
        } else {
          isError = true;
          isLoading = false;
          _staffDetail = staffDetail;
          debugPrint(staffDetail.error!.message);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return DialogMessage(message: staffDetail.error!.message!);
              });
        }
      });
    }
  }

  void openLightBox(BuildContext context, String image) {
    LightBox.openLightBox(
      context,
      child: GalleryPhotoViewWrapper(
        galleryItems: [image],
        backgroundDecoration: const BoxDecoration(
          color: Color(0x99000000),
        ),
        initialIndex: 0,
      ),
    );
  }

  void nextCheckIn() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CameraCheck(
          type: TypeCheckConst.checkin,
          userId: widget.staffs.id!,
        ),
      ),
    ) as Map<String, dynamic>?;
    // final Map<String, dynamic> data =
    //     Map<String, dynamic>.from(json.decode(result.toString()));
    debugPrint(result.runtimeType.toString());
    if (result != null) {
      loadDetail();

      if (result['status'] == false) {
        showErrorDialog(context, text: result['message'], dismissible: true);
      } else if (result['status'] == true) {
        showDialogResult(context, text: result['method']);
      }
    }
  }

  void nextCheckOut() async {
    final result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => CameraCheck(
          type: TypeCheckConst.checkout,
          userId: widget.staffs.id!,
        ),
      ),
    ) as Map<String, dynamic>?;
    // final Map<String, dynamic> data =
    //     Map<String, dynamic>.from(json.decode(result.toString()));
    debugPrint(result.runtimeType.toString());
    if (result != null) {
      loadDetail();
      if (result['status'] == false) {
        showErrorDialog(context, text: result['message'], dismissible: true);
      } else if (result['status'] == true) {
        showDialogResult(context, text: result['method']);
      }
    }
  }

  Color buildColorTitle(String method) {
    Color color = Colors.black;
    switch (method) {
      case 'EARLY':
        color = const Color(0xFFFF8300);
        break;
      case 'LATE':
        color = const Color(0xFFFF8300);
        break;
      case 'VALID':
        color = const Color(0xFF3AB35A);
        break;
      default:
        color = Colors.black;
        break;
    }
    return color;
  }

  String handleMethod(String method) {
    switch (method) {
      case 'VALID':
        return 'Hợp lệ';
      case 'LATE':
        return 'Đi muộn';
      case 'EARLY':
        return 'Về sớm';
      default:
        return '';
    }
  }

  String time(String time) {
    var dateTime = DateTime.parse("2022-05-22 " + time);
    final format = DateFormat('HH:mm');
    return format.format(dateTime);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.refresh),
        onPressed: () {
          setState(() {
            isLoading = true;
          });
          loadDetail();
        },
      ),
      appBar: AppBar(
        title: Text(
          "ID: " + widget.staffs.id.toString(),
        ),
      ),
      body: isLoading
          ? const Center(
              child: Indicator(),
            )
          : isError
              ? const SizedBox()
              : SingleChildScrollView(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 100,
                        height: 100,
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: widget.staffs.urlAvatar,
                          errorWidget: (_, url, error) =>
                              Image.asset('assets/logo/logo.png'),
                          placeholder: (_, url) =>
                              const CupertinoActivityIndicator(),
                        ),
                      ),
                      const SizedBox(
                        height: 8,
                      ),
                      Text(
                        (widget.staffs.name ?? '') +
                            " - " +
                            (widget.staffs.code ?? ''),
                        style: const TextStyle(fontWeight: FontWeight.bold),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(widget.staffs.department ?? ''),
                      const SizedBox(
                        height: 24.0,
                      ),
                      _staffDetail!.checkIn == null
                          ? AppButton(
                              icon: const Icon(Icons.login),
                              width: double.infinity,
                              label: 'Check-in',
                              onPressed: () {
                                nextCheckIn();
                              },
                            )
                          : InkWell(
                              onTap: () {
                                openLightBox(context,
                                    _staffDetail!.checkIn!.image!.urlImage);
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                width: MediaQuery.of(context).size.width,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: buildColorTitle(
                                      _staffDetail!.checkIn!.method!),
                                ),
                                child: Column(
                                  children: [
                                    Text(
                                      'Đã check in lúc '
                                      '${_staffDetail!.checkIn!.realTime}',
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      handleMethod(
                                              _staffDetail!.checkIn!.method!) +
                                          " - " +
                                          "Bấm xem ảnh",
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                      const SizedBox(
                        height: 12.0,
                      ),
                      _staffDetail!.checkOut == null
                          ? AppButton(
                              icon: const Icon(Icons.logout),
                              width: double.infinity,
                              label: 'Check-out',
                              onPressed: () {
                                nextCheckOut();
                              },
                            )
                          : InkWell(
                              onTap: () {
                                openLightBox(context,
                                    _staffDetail!.checkOut!.image!.urlImage);
                              },
                              child: Container(
                                padding: const EdgeInsets.all(8.0),
                                width: MediaQuery.of(context).size.width,
                                height: 50.0,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  color: buildColorTitle(
                                      _staffDetail!.checkOut!.method!),
                                ),
                                child: Column(
                                  children: [
                                    Text(
                                      'Đã check out lúc '
                                      '${_staffDetail!.checkOut!.realTime}',
                                      textAlign: TextAlign.center,
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Text(
                                      handleMethod(
                                              _staffDetail!.checkOut!.method!) +
                                          " - " +
                                          "Bấm xem ảnh",
                                      style: const TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                      const SizedBox(
                        height: 24.0,
                      ),
                      const Text(
                        'Thời gian làm việc hôm nay của bạn:',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Column(
                        children: _staffDetail!.timeBlocks!
                            .map(
                              (e) => Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Text(
                                    time(e.timeIn!) + " - " + time(e.timeOut!)),
                              ),
                            )
                            .toList(),
                      ),

                      if (!_staffDetail!.isValid) ...[
                        const SizedBox(
                          height: 8.0,
                        ),
                        const Text(
                          'Phiếu xin thay đổi lịch làm việc hôm nay của bạn',
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        const SizedBox(
                          height: 8.0,
                        ),
                      ],
                      Column(
                        children: _staffDetail!.absence!
                            .map((e) => Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: [
                                      Text('PTD${e.id} Xin nghỉ'),
                                      const SizedBox(
                                        width: 8.0,
                                      ),
                                      TagStatus(
                                        status: e.status!,
                                      ),
                                    ],
                                  ),
                                ))
                            .toList(),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Column(
                        children: _staffDetail!.outside!
                            .map((e) => Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    children: [
                                      Text('PTD${e.id} Làn ngoài'),
                                      const SizedBox(
                                        width: 8.0,
                                      ),
                                      TagStatus(
                                        status: e.status!,
                                      ),
                                    ],
                                  ),
                                ))
                            .toList(),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Column(
                        children: _staffDetail!.outside!
                            .map((e) => Padding(
                                  padding: const EdgeInsets.only(bottom: 8.0),
                                  child: Row(
                                    children: [
                                      Text('PTD${e.id} Làm thêm giờ'),
                                      const SizedBox(
                                        width: 8.0,
                                      ),
                                      TagStatus(
                                        status: e.status!,
                                      ),
                                    ],
                                  ),
                                ))
                            .toList(),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Column(
                        children: _staffDetail!.timeKeepingTicket!
                            .map(
                              (e) => Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                        '${e.txtForType} - ${e.minute.toString()} phút'),
                                    const SizedBox(
                                      width: 8.0,
                                    ),
                                    TagStatus(
                                      status: e.status!,
                                    ),
                                  ],
                                ),
                              ),
                            )
                            .toList(),
                      ),
                      const SizedBox(
                        height: 24.0,
                      ),
                      Text(
                        "Thông tin chấm công THÁNG " +
                            Jiffy().month.toString() +
                            " của bạn",
                        style: const TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(
                          'Đi muộn: ${_staffDetail!.lateCount!.toString()} lần (${_staffDetail!.lateHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(
                          'Về sớm: ${_staffDetail!.earlyCount!.toString()} lần (${_staffDetail!.earlyHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 16.0,
                      ),
                      Text(
                          'Đi muộn có phép: ${_staffDetail!.lateTicketCount!.toString()} lần (${_staffDetail!.lateTicketHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(
                          'Về sớm có phép: ${_staffDetail!.earlyTicketCount!.toString()} lần (${_staffDetail!.earlyTicketHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 16.0,
                      ),
                      Text(
                          'Nghỉ có phép: ${_staffDetail!.absenceAllowedDay!.toString()} ngày (${_staffDetail!.absenceAllowedHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 8.0,
                      ),
                      Text(
                          'Nghỉ không phép: ${_staffDetail!.absenceNotAllowedDay!.toString()} ngày (${_staffDetail!.absenceNotAllowedHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 8.0,
                      ),
                      const SizedBox(
                        height: 16.0,
                      ),
                      Text(
                          'Số lần chấm công bù: ${_staffDetail!.compensationCount!.toString()} lần / 6 lần'),
                      const SizedBox(
                        height: 8.0,
                      ),
                      // Text(
                      // 'Số ngày làm việc không chấm công: ${_staffDetail!.userNotWorkingCount!.toString()} lần (${_staffDetail!.userNotWorkingHour!.toString()} giờ)'),
                      const SizedBox(
                        height: 24.0,
                      ),
                      Text(
                          'Tổng số giờ làm việc: ${_staffDetail!.userWorkingHour!.toString()} giờ'),
                      const SizedBox(
                        height: 80.0,
                      ),
                    ],
                  ),
                ),
    );
  }

  Widget errorWidget(String msg) {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            AppButton(
              icon: const Icon(Icons.error),
              width: double.infinity,
              label: 'Xem chi tiết lỗi',
              onPressed: () {
                setState(() {
                  showError = !showError;
                });
              },
            ),
            const SizedBox(
              height: 12.0,
            ),
            showError ? Text(msg) : const SizedBox()
          ],
        ),
      ),
    );
  }
}
