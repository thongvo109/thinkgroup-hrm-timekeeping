import 'package:after_layout/after_layout.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/collections/staffs_collection.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_detail_model.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_model.dart';
import 'package:thinkgroup_timekeeping_app/repositories/staffs_repository.dart';
import 'package:thinkgroup_timekeeping_app/screens/detail.dart';
import 'package:thinkgroup_timekeeping_app/widgets/dialog.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AfterLayoutMixin {
  List<StaffsModel>? list = [];
  List<StaffsModel>? search;
  final TextEditingController _controller = TextEditingController();
  @override
  void initState() {
    super.initState();
  }

  @override
  void afterFirstLayout(BuildContext context) {
    loadList();
  }

  void loadList() async {
    setState(() {
      list = [];
      search = null;
    });
    StaffsCollection? response = await StaffsReposioty().loadList();

    setState(() {
      if (response!.error == null) {
        list = response.items as List<StaffsModel>?;
        // list = lista!.where((element) => element.checkIn != null).toList();
      } else {
        list = [];
      }
    });
  }

  Future<void> searchHandle(String value) async {
    setState(() {
      if (list!.isNotEmpty) {
        if (_controller.text.isNotEmpty) {
          var _list = list!
              .where((element) => element.code
                  .toString()
                  .toLowerCase()
                  .contains(value.toLowerCase()))
              .toList();

          if (_list.isEmpty) {
            search = null;
          } else {
            search = null;
          }
        } else {
          search = null;
        }
      }
    });
  }

  void loadDetail(int id, StaffsModel item) async {
    StaffDetailModel? staffDetail = await StaffsReposioty().loadDetail(id);

    if (staffDetail!.error == null) {
      await Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => StaffDetailScreen(
                    staffs: item,
                  ))).whenComplete(() {
        loadList();
        _controller.clear();
      });
    } else {
      debugPrint(staffDetail.error!.message);
      showDialog(
          context: context,
          builder: (BuildContext context) {
            return DialogMessage(message: staffDetail.error!.message!);
          });
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.refresh),
        onPressed: () {
          loadList();
        },
      ),
      appBar: AppBar(
        title: const Text('Hệ thống điểm danh'),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(FocusNode());
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
              child: TextField(
                controller: _controller,
                onChanged: searchHandle,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  hintText: 'Nhập mã.',
                  suffixIcon: IconButton(
                    icon: const Icon(Icons.clear),
                    onPressed: () {
                      _controller.clear();
                      setState(() {
                        search = [];
                      });
                    },
                  ),
                ),
              ),
            ),
            Expanded(
              child: ListView.separated(
                separatorBuilder: (_, index) => const SizedBox(
                  height: 10,
                  child: Divider(),
                ),
                padding:
                    const EdgeInsets.symmetric(vertical: 8.0, horizontal: 16.0),
                itemCount: search != null ? search!.length : list!.length,
                itemBuilder: (BuildContext context, int index) {
                  var item = search != null ? search![index] : list![index];
                  // print(item.checkIn!.toJson().toString());
                  return InkWell(
                    onTap: () async {
                      loadDetail(item.id!, item);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                          width: 50,
                          height: 50,
                          child: CachedNetworkImage(
                            fit: BoxFit.cover,
                            imageUrl: item.urlAvatar,
                            errorWidget: (_, url, error) =>
                                Image.asset('assets/logo/logo.png'),
                            placeholder: (_, url) =>
                                const CupertinoActivityIndicator(),
                          ),
                        ),
                        const SizedBox(
                          width: 8.0,
                        ),
                        Expanded(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              RichText(
                                text: TextSpan(
                                  style: const TextStyle(
                                    fontSize: 12.0,
                                    color: Colors.black,
                                    height: 1.5,
                                  ),
                                  children: [
                                    TextSpan(
                                      text: (item.name ?? '') +
                                          " - " +
                                          item.department!,
                                    ),
                                    TextSpan(
                                        text: item.code == null ? '' : ' - '),
                                    TextSpan(
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold),
                                      text: item.code ?? '',
                                    ),
                                  ],
                                ),
                              ),
                              const SizedBox(
                                height: 8.0,
                              ),
                              item.checkIn == null
                                  ? const SizedBox()
                                  : Text(
                                      'IN: Đã check-in (${item.checkIn!.realTime})',
                                      style: const TextStyle(
                                          fontSize: 12.0, color: Colors.grey),
                                    ),
                              const SizedBox(
                                height: 8.0,
                              ),
                              item.checkOut == null
                                  ? const SizedBox()
                                  : Text(
                                      'OUT: Đã check-out (${item.checkOut!.realTime})',
                                      style: const TextStyle(
                                        fontSize: 12.0,
                                        color: Colors.grey,
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
