import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:thinkgroup_timekeeping_app/provider/camera_check_provider.dart';
import 'package:thinkgroup_timekeeping_app/resoures/app_color.dart';
import 'package:thinkgroup_timekeeping_app/widgets/button.dart';

class CameraCheck extends StatelessWidget {
  final int type;
  final int userId;
  const CameraCheck({Key? key, required this.type, required this.userId})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (context) => CameraVM(
        context: context,
        userId: userId,
        isCheckin: type,
      ),
      child: Scaffold(
        appBar: AppBar(),
        body: Padding(
          padding: const EdgeInsets.all(16),
          child: Column(
            children: [
              Expanded(child: _buildCamera),
              const SizedBox(height: 25),
              _buildButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildButton() {
    return Selector<CameraVM, int>(
      builder: (context, state, child) {
        return AppButton(
          onPressed: () {
            context.read<CameraVM>().onTakePhoto();
          },
          width: double.infinity,
          disableTextColor: AppColor.white,
          enableTextColor: AppColor.white,
          label: 'CHỤP',
          color: AppColor.blueLight,
          disableColor: AppColor.blueLight,
        );
      },
      selector: (_, provider) => provider.value,
    );
  }

  Widget get _buildCamera => Selector<CameraVM, CameraController?>(
        selector: (context, provider) => provider.controller,
        builder: (context, controller, _) => ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: controller == null
              ? Container(color: AppColor.black)
              : Stack(
                  children: [
                    CameraPreview(controller),
                    // Positioned(
                    //   right: 10,
                    //   top: 10,
                    //   child: IconButton(
                    //     onPressed: () {
                    //       context.read<CheckInVM>().changeCameraDescription();
                    //     },
                    //     icon: const Icon(
                    //       Icons.cameraswitch,
                    //       color: Colors.white,
                    //       size: 34,
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
        ),
      );
}
