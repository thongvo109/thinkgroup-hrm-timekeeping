import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/models/base_model.dart';

class BaseCollection {
  final bool? success;
  final String? message;
  final List<BaseModel>? items;
  final ApiError? error;
  BaseCollection({
    this.success,
    this.message,
    this.items,
    this.error,
  });

  BaseCollection.withError(ApiError _error)
      : items = [],
        message = '',
        success = false,
        error = _error;
}
