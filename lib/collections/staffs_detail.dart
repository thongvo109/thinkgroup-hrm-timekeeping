import 'package:thinkgroup_timekeeping_app/models/staffs_detail_model.dart';

class StaffsDetail {
  final bool? success;
  final String? mesage;
  final StaffDetailModel? data;

  StaffsDetail({
    this.success,
    this.mesage,
    this.data,
  });

  StaffsDetail.fromJson(Map<String, dynamic> json)
      : success = json['success'],
        mesage = json['message'],
        data = json['data'] == null
            ? null
            : StaffDetailModel.fromJson(json['data']);
}
