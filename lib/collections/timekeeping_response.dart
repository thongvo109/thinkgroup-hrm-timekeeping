import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/collections/base_response.dart';
import 'package:thinkgroup_timekeeping_app/models/timekeeping_model.dart';

class TimeKeepingResponse extends BaseResponse {
  TimeKeepingResponse.fromJson(Map<String, dynamic> json)
      : super(
          data: json['data'] == null
              ? null
              : TimeKeepingModel.fromJson(json['data']),
          success: json['success'],
          mesage: json['message'],
          error: null,
        );

  TimeKeepingResponse.withError(ApiError apiError) : super.withError(apiError);
}
