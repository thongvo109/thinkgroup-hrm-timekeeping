import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/collections/base_collection.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_model.dart';

class StaffsCollection extends BaseCollection {
  StaffsCollection.fromJson(Map<String, dynamic> json)
      : super(
          items: json['data'] == null
              ? <StaffsModel>[]
              : (json['data'] as List)
                  .map((e) => StaffsModel.fromJson(e))
                  .toList(),
          message: json['message'],
          success: json['success'],
          error: null,
        );

  StaffsCollection.withError(ApiError errorDetail)
      : super.withError(errorDetail);
}
