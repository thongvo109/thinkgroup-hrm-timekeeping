import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/models/base_model.dart';

typedef TypeData<T> = T Function(Map<String, dynamic>);

class BaseResponse {
  final bool? success;
  final String? mesage;
  final BaseModel? data;
  final ApiError? error;
  BaseResponse({
    this.success,
    this.mesage,
    this.data,
    this.error,
  });

  BaseResponse.withError(ApiError errorDetail)
      : success = false,
        mesage = '',
        data = null,
        error = errorDetail;
}
