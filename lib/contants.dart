class TypeCheckConst {
  static const checkin = 1;
  static const checkout = 3;
}

class TimeKeepingMethod {
  static const valid = "VALID";
  static const early = "EARLY";
  static const last = "LATE";
  static const forgot = "FORGOT";
  static const noCheckIn = "NO_CHECKIN";
  static const nOCheckOut = "NO_CHECKOUT";
  static const allowedEarly = "ALLOWED_EARLY";
  static const allowedLate = "ALLOWED_LATE";
}
