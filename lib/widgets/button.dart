import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/resoures/app_color.dart';

class AppButton extends StatelessWidget {
  final VoidCallback? onPressed;
  final double? width;
  final Widget? icon;
  final String label;
  final Color color;
  final Color disableColor;
  final Color enableTextColor;
  final Color disableTextColor;
  final EdgeInsets padding;
  final OutlinedBorder? enableBorder;
  final OutlinedBorder? disableBorder;
  final bool toUpperCase;
  final FontWeight fontWeight;
  bool get enable => onPressed != null;

  const AppButton({
    Key? key,
    this.onPressed,
    this.width,
    this.color = AppColor.blue1,
    this.enableTextColor = AppColor.white,
    this.disableTextColor = AppColor.blue1,
    this.icon,
    this.enableBorder,
    this.disableBorder,
    this.disableColor = AppColor.white,
    required this.label,
    this.toUpperCase = true,
    this.padding = const EdgeInsets.symmetric(vertical: 15),
    this.fontWeight = FontWeight.w500,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: width,
      child: ElevatedButton(
        onPressed: () {
          onPressed?.call();
        },
        child: icon != null
            ? Row(
                mainAxisSize: MainAxisSize.min,
                children: [
                  icon!,
                  const SizedBox(width: 6),
                  Text(
                    toUpperCase ? label.toUpperCase() : label,
                    style: TextStyle(
                        fontSize: 14,
                        fontWeight: fontWeight,
                        color: enable ? enableTextColor : disableTextColor),
                  )
                ],
              )
            : Text(
                toUpperCase ? label.toUpperCase() : label,
                style: TextStyle(
                    fontSize: 14,
                    fontWeight: fontWeight,
                    color: enable ? enableTextColor : disableTextColor),
              ),
        style: ElevatedButton.styleFrom(
            primary: enable ? color : disableColor,
            onSurface: enable ? color : disableColor,
            elevation: 0,
            shape: enable ? enableBorder : disableBorder,
            alignment: Alignment.center,
            textStyle: TextStyle(
                fontSize: 14,
                fontWeight: FontWeight.w500,
                color: enable ? enableTextColor : disableTextColor),
            padding: padding),
      ),
    );
  }
}
