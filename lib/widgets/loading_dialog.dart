import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/widgets/indicator.dart';

class LoadingDialog extends StatelessWidget {
  final String text;

  const LoadingDialog({
    Key? key,
    this.text = 'Loading',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      contentPadding: const EdgeInsets.fromLTRB(20.0, 0.0, 20.0, 0.0),
      content: SizedBox(
        width: 250.0,
        height: 100.0,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Indicator(
              size: 16,
            ),
            const SizedBox(
              width: 20,
            ),
            Expanded(
              child: Text(
                text,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
