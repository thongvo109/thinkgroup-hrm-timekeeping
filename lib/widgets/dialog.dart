import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/resoures/app_color.dart';

class DialogMessage extends StatelessWidget {
  final String message;
  const DialogMessage({Key? key, this.message = ''}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Text(
        message,
        style: TextStyle(fontSize: 14, color: AppColor.black.withOpacity(0.6)),
      ),
      actions: [
        TextButton(
          onPressed: Navigator.of(context).pop,
          child: const Text(
            'Đóng',
            style: TextStyle(
                color: AppColor.neutral3,
                fontSize: 14,
                fontWeight: FontWeight.w500),
          ),
        )
      ],
    );
  }
}
