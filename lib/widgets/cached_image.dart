import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';

class Avatar extends StatelessWidget {
  final String path;
  const Avatar({Key? key, required this.path}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: path,
      errorWidget: (ctx, _, error) => const Placeholder(),
      fit: BoxFit.cover,
    );
  }
}
