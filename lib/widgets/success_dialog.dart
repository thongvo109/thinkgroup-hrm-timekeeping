import 'package:flutter/material.dart';

class SuccessDialog extends StatelessWidget {
  final String text;

  const SuccessDialog({
    Key? key,
    this.text = 'Succeed.',
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      contentPadding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      content: SizedBox(
        width: 250.0,
        height: 200.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Icon(
              Icons.check_circle,
              size: 64,
              color: Color(0xFF52B24D),
            ),
            const SizedBox(
              height: 20,
            ),
            Text(
              text,
            ),
          ],
        ),
      ),
    );
  }
}
