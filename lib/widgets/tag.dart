import 'package:flutter/material.dart';

class TagStatus extends StatelessWidget {
  final int status;
  const TagStatus({Key? key, required this.status}) : super(key: key);

  String txtStatus() {
    switch (status) {
      case -1:
        return 'Đã hủy';
      case 0:
        return 'Chờ duyệt';
      case 1:
        return 'Đã duyệt';
      case 2:
        return 'Đã hạn xử lý';

      default:
        return '';
    }
  }

  Color colorStatus() {
    switch (status) {
      case -1:
        return Colors.redAccent;
      case 0:
        return Colors.orangeAccent;
      case 1:
        return const Color(0xFF5cb85c);
      case 2:
        return Colors.yellowAccent;

      default:
        return Colors.grey;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: colorStatus(), borderRadius: BorderRadius.circular(8.0)),
      padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 8.0),
      child: Text(
        txtStatus(),
        style: const TextStyle(
          color: Colors.white,
        ),
      ),
    );
  }
}
