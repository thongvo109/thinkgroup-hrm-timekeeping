import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/contants.dart';

class ResultDialog extends StatelessWidget {
  final String method;
  const ResultDialog({Key? key, required this.method}) : super(key: key);

  String buildTitle() {
    switch (method) {
      case TimeKeepingMethod.early:
        return 'Bạn về sớm';

      case TimeKeepingMethod.last:
        return "Bạn đi muộn!";
      case TimeKeepingMethod.valid:
        return "Chấm công hợp lệ";
      case TimeKeepingMethod.allowedEarly:
        return "Được phép về sớm";
      case TimeKeepingMethod.allowedLate:
        return "Được phép đi muộn";
      default:
        return "";
    }
  }

  Color buildColorIcon() {
    switch (method) {
      case TimeKeepingMethod.early:
        return const Color(0xFFFF8300);
      case TimeKeepingMethod.last:
        return const Color(0xFFFF8300);
      case TimeKeepingMethod.valid:
        return const Color(0xFF3AB35A);
      case TimeKeepingMethod.allowedEarly:
        return const Color(0xFF3AB35A);
      case TimeKeepingMethod.allowedLate:
        return const Color(0xFF3AB35A);
      default:
        return Colors.black;
    }
  }

  IconData? get buildIcon {
    switch (method) {
      case TimeKeepingMethod.early:
        return Icons.alarm;
      case TimeKeepingMethod.last:
        return Icons.warning;
      case TimeKeepingMethod.valid:
        return Icons.check_circle;
      case TimeKeepingMethod.allowedLate:
        return Icons.check_circle;
      case TimeKeepingMethod.allowedEarly:
        return Icons.check_circle;
      default:
        return null;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
      ),
      contentPadding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 0.0),
      content: SizedBox(
        width: 250.0,
        height: 200.0,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            buildIcon == null
                ? const SizedBox()
                : Icon(
                    buildIcon,
                    size: 64,
                    color: buildColorIcon(),
                  ),
            const SizedBox(
              height: 20,
            ),
            Text(
              buildTitle(),
            ),
          ],
        ),
      ),
    );
  }
}
