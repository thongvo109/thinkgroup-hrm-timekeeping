import 'package:flutter/material.dart';

class LightBox {
  static openLightBox(BuildContext context, {Widget? child}) {
    showGeneralDialog(
      barrierColor: Colors.black12.withOpacity(0.6),
      barrierDismissible: false,
      transitionDuration: const Duration(milliseconds: 400),
      context: context,
      pageBuilder: (BuildContext context, Animation<double> animation,
          Animation<double> secondaryAnimation) {
        return Material(
          type: MaterialType.transparency,
          child: Center(
            // Aligns the container to center
            child: SizedBox.expand(
              child: child,
            ),
          ),
        );
      },
    );
  }
}
