import 'package:flutter/material.dart';
import 'package:thinkgroup_timekeeping_app/resoures/app_color.dart';

class Indicator extends StatelessWidget {
  final double? size;
  final Color? color;

  const Indicator({Key? key, this.size, this.color}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: SizedBox(
        width: size ?? 16.0,
        height: size ?? 16.0,
        child: CircularProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(color ?? AppColor.primary),
        ),
      ),
    );
  }
}
