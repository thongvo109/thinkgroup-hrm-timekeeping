class BuildConfig {
  static const baseUrl = 'thinkpro.vn';
  // static const baseUrlApi = 'https://api-staging.$baseUrl';
  static const baseUrlApi = 'https://api-beta.$baseUrl';
  // static const baseUrlApi = 'https://api-qc.$baseUrl';
  static const baseMedia = 'https://mediax.$baseUrl';
  static const baseUrlApiCMS = '$baseUrlApi/cms';
  static const baseUrlApiHRM = '$baseUrlApi/hrm';
  static const baseUrlApiAccountant = '$baseUrlApi/accountant';
  static const baseUrlApp = '$baseUrlApi/app';
  static const debug = false;
}
