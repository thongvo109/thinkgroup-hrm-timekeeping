import 'dart:io';

import 'package:camera/camera.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'package:thinkgroup_timekeeping_app/contants.dart';
import 'package:thinkgroup_timekeeping_app/mixins/dialog.dart';
import 'package:thinkgroup_timekeeping_app/models/timekeeping_model.dart';
import 'package:thinkgroup_timekeeping_app/repositories/staffs_repository.dart';

class CameraVM extends ChangeNotifier with DialogMixin {
  final BuildContext context;
  final int userId;
  final int ahihi = 20;
  CameraController? controller;
  VoidCallback? onSuccess;
  late XFile fileRawResult;
  int selectedCamera = 1;
  bool isProcess = false;
  int isCheckin;
  List<CameraDescription> camera = [];
  CameraVM(
      {required this.context, required this.userId, required this.isCheckin}) {
    WidgetsBinding.instance?.addPostFrameCallback((timeStamp) {
      availableCameras().then((value) {
        if (value.isEmpty) return;
        camera = value;
        controller =
            CameraController(camera[selectedCamera], ResolutionPreset.max);
        controller?.initialize().then((_) {
          notifyListeners();
        });
      });
    });
    dialog();
  }
  void dialog() async {}

  int get value => ahihi;

  void test() {
    debugPrint(ahihi.toString());
  }

  Future<void> onTakePhoto() async {
    showDialogLoading(context);
    notifyListeners();
    try {
      final fileRaw = await controller?.takePicture();
      if (fileRaw != null) {
        fileRawResult = fileRaw;
        handleCheckIn();
        notifyListeners();
      }
    } catch (_) {
      hideDialog(context);
      controller?.resumePreview();

      return;
    }
  }

  Future<void> handleCheckIn() async {
    controller?.pausePreview();
    // showLoading();
    File? _file = await _compressAndGetFile(fileRawResult);
    if (_file != null) {
      await submitCheckIn(_file);
    } else {
      return;
    }
  }

  Future<void> submitCheckIn(File? file) async {
    controller!.pausePreview();
    FormData body = FormData.fromMap({
      "user_id": userId,
      "image": MultipartFile.fromFileSync(file!.path, filename: file.path),
    });

    final response = await StaffsReposioty().check(
        formData: body,
        checkin: isCheckin == TypeCheckConst.checkin ? true : false);
    hideDialog(context);
    if (response.error == null) {
      TimeKeepingModel timeKeeping = response.data as TimeKeepingModel;
      debugPrint(
        "response check in success: " +
            (response.data as TimeKeepingModel).toJson().toString(),
      );
      Navigator.pop(context, {
        "status": true,
        "message": timeKeeping.method,
      });
    } else {
      debugPrint(response.error!.message);
      Navigator.pop(context, {
        "status": false,
        "message": response.error!.message,
      });
    }
  }

  Future<File?> _compressAndGetFile(XFile file) async {
    final dir = await path_provider.getTemporaryDirectory();
    final targetPath = '${dir.absolute.path}/temp.jpg';
    var result = await FlutterImageCompress.compressAndGetFile(
        file.path, targetPath,
        quality: 50);

    return result;
  }
}
