import 'package:thinkgroup_timekeeping_app/api/api_error.dart';

class BaseModel {
  ApiError? error;

  BaseModel({this.error});

  BaseModel.withError(ApiError _error) : error = _error;
}
