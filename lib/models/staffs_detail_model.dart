import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/models/base_model.dart';
import 'package:thinkgroup_timekeeping_app/models/staffs_model.dart';

class StaffDetailModel extends BaseModel {
  final DateBlockModel? dateBlock;
  final List<TimeBlockModel>? timeBlocks;
  final int? earlyCount;
  final num? earlyHour;
  final int? lateCount;
  final num? lateHour;
  final int? compensationCount;
  final int? userNotWorkingCount;
  final num? userNotWorkingHour;
  final int? userWorkingCount;
  final num? userWorkingHour;
  final int? lateTicketCount;
  final int? earlyTicketCount;
  final num? lateTicketHour;
  final num? earlyTicketHour;
  final List<TimeKeepingTicket>? timeKeepingTicket;
  final List<OverTimeModel>? overTime;
  final List<OutsideModel>? outside;
  final List<AbsenceModel>? absence;
  final TypeCheck? checkIn;
  final TypeCheck? checkOut;
  final num? absenceAllowedHour;
  final num? absenceAllowedDay;
  final num? absenceNotAllowedHour;
  final num? absenceNotAllowedDay;

  bool get isValid =>
      timeKeepingTicket!.isEmpty &&
      overTime!.isEmpty &&
      outside!.isEmpty &&
      absence!.isEmpty;

  StaffDetailModel({
    this.earlyHour,
    this.lateHour,
    this.dateBlock,
    this.timeBlocks,
    this.earlyCount,
    this.lateCount,
    this.compensationCount,
    this.userNotWorkingCount,
    this.userNotWorkingHour,
    this.userWorkingCount,
    this.userWorkingHour,
    this.earlyTicketCount,
    this.lateTicketCount,
    this.lateTicketHour,
    this.earlyTicketHour,
    this.timeKeepingTicket,
    this.overTime,
    this.outside,
    this.absence,
    this.checkIn,
    this.checkOut,
    this.absenceAllowedDay,
    this.absenceAllowedHour,
    this.absenceNotAllowedDay,
    this.absenceNotAllowedHour,
  });

  StaffDetailModel.fromJson(Map<String, dynamic> json)
      : dateBlock = json['dateBlock'] == null
            ? null
            : DateBlockModel.fromJson(json['dateBlock']),
        timeBlocks = json['timeBlock'] == []
            ? <TimeBlockModel>[]
            : ((json['timeBlock'] ?? []) as List)
                .map((e) => TimeBlockModel.fromJson(e))
                .toList(),
        earlyCount = json['early_count'],
        lateCount = json['late_count'],
        lateHour = json['late_hour'],
        earlyHour = json['early_hour'],
        compensationCount = json['compensation_count'],
        userNotWorkingCount = json['user_not_working_count'],
        userNotWorkingHour = json['user_not_working_hour'],
        userWorkingCount = json['user_working_count'],
        userWorkingHour = json['user_working_hour'],
        earlyTicketCount = json['early_ticket_count'],
        lateTicketCount = json['late_ticket_count'],
        earlyTicketHour = json['early_ticket_hour'],
        lateTicketHour = json['late_ticket_hour'],
        absenceAllowedHour = json['absence_allowed_hour'],
        absenceAllowedDay = json['absence_allowed_day'],
        absenceNotAllowedDay = json['absence_not_allowed_day'],
        absenceNotAllowedHour = json['absence_not_allowed_hour'],
        timeKeepingTicket = json['timekeeping_ticket'] == []
            ? <TimeKeepingTicket>[]
            : (json['timekeeping_ticket'] as List)
                .map((e) => TimeKeepingTicket.fromJson(e))
                .toList(),
        overTime = json['overtime'] == []
            ? <OverTimeModel>[]
            : ((json['overtime'] ?? []) as List)
                .map((e) => OverTimeModel.fromJson(e))
                .toList(),
        outside = json['outsides'] == []
            ? <OutsideModel>[]
            : ((json['outsides'] ?? []) as List)
                .map((e) => OutsideModel.fromJson(e))
                .toList(),
        absence = json['absences'] == []
            ? <AbsenceModel>[]
            : ((json['absences'] ?? []) as List)
                .map((e) => AbsenceModel.fromJson(e))
                .toList(),
        checkIn = json['checkin'] == null
            ? null
            : TypeCheck.fromJson(json['checkin']),
        checkOut = json['checkout'] == null
            ? null
            : TypeCheck.fromJson(json['checkout']);

  StaffDetailModel.withError(ApiError errorDetail)
      : dateBlock = null,
        lateHour = 0,
        earlyHour = 0,
        timeBlocks = [],
        earlyCount = 0,
        lateCount = 0,
        compensationCount = 0,
        userNotWorkingCount = 0,
        userNotWorkingHour = 0,
        userWorkingCount = 0,
        userWorkingHour = 0,
        earlyTicketCount = 0,
        lateTicketCount = 0,
        lateTicketHour = 0,
        earlyTicketHour = 0,
        absenceAllowedDay = 0,
        absenceAllowedHour = 0,
        absenceNotAllowedDay = 0,
        absenceNotAllowedHour = 0,
        timeKeepingTicket = [],
        overTime = [],
        outside = [],
        absence = [],
        checkIn = null,
        checkOut = null,
        super.withError(errorDetail);
}

class DateBlockModel {
  final int? id;
  final int? userId;
  final String? date;
  final bool? isSupperSunday;
  final dynamic holiday;
  final String? timeIn;
  final String? timeOut;
  final int? status;
  final int? type;
  final String? note;
  final int? createdByid;
  final String? createdAt;

  DateBlockModel({
    this.id,
    this.userId,
    this.date,
    this.isSupperSunday,
    this.holiday,
    this.timeIn,
    this.timeOut,
    this.status,
    this.type,
    this.note,
    this.createdByid,
    this.createdAt,
  });

  DateBlockModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        userId = json['user_id'],
        date = json['date'],
        isSupperSunday = json['is_super_sunday'],
        holiday = json['holiday'],
        timeIn = json['time_in'],
        timeOut = json['time_out'],
        status = json['status'],
        type = json['type'],
        note = json['note'] ?? '',
        createdByid = json['created_by_id'],
        createdAt = json['crearted_at'];
}

class TimeBlockModel {
  final int? id;
  final String? timeIn;
  final String? timeOut;
  final num? hourNumber;
  final num? wageWeight;
  final BlockType? blockType;
  final String? createdDate;

  TimeBlockModel({
    this.id,
    this.timeIn,
    this.timeOut,
    this.hourNumber,
    this.wageWeight,
    this.blockType,
    this.createdDate,
  });

  TimeBlockModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        timeIn = json['time_in'],
        timeOut = json['time_out'],
        hourNumber = json['hour_number'],
        wageWeight = json['wage_weight'],
        blockType = json['block_type'] == null
            ? null
            : BlockType.fromJson(json['block_type']),
        createdDate = json['created_at'] ?? '';
}

class BlockType {
  final int? id;
  final String? name;
  final String? color;
  final String? bgColor;
  final String? description;
  final num? wageWeight;

  BlockType({
    this.id,
    this.name,
    this.color,
    this.bgColor,
    this.description,
    this.wageWeight,
  });

  BlockType.fromJson(Map<String, dynamic> json)
      : id = json['id'] ?? 0,
        name = json['name'] ?? '',
        color = json['color'] ?? '',
        bgColor = json['bg_color'] ?? '',
        description = json['description'] ?? '',
        wageWeight = json['wage_weight'] ?? 0;
}

class AbsenceModel {
  final int? id;
  final List<TimeBlockModel>? timeBlocks;
  final num? workHour;
  final String? reason;
  final int? status;
  final String? note;
  AbsenceModel({
    this.id,
    this.timeBlocks,
    this.workHour,
    this.reason,
    this.status,
    this.note,
  });

  AbsenceModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        timeBlocks = json['time_blocks'] == null
            ? null
            : (json['time_blocks'] as List)
                .map((e) => TimeBlockModel.fromJson(e))
                .toList(),
        workHour = json['work_hour'],
        reason = json['reason'],
        status = json['status'],
        note = json['note'];
}

class OutsideModel {
  final int? id;
  final List<TimeBlockModel>? timeBlocks;
  final num? workHour;
  final int? type;
  final String? location;
  final String? reason;
  final int? status;
  final dynamic note;
  final String? createdAt;

  OutsideModel({
    this.id,
    this.timeBlocks,
    this.workHour,
    this.type,
    this.location,
    this.reason,
    this.status,
    this.note,
    this.createdAt,
  });

  OutsideModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        timeBlocks = json['time_blocks'] == null
            ? null
            : (json['time_blocks'] as List)
                .map((e) => TimeBlockModel.fromJson(e))
                .toList(),
        workHour = json['work_hour'],
        type = json['type'],
        location = json['location'],
        reason = json['reason'],
        status = json['status'],
        note = json['note'],
        createdAt = json['created_at'];
}

class OverTimeModel {
  final String? date;
  final List<dynamic>? period;
  final String? reason;
  final dynamic images;
  final int? status;
  final dynamic note;
  final String? createdAt;

  OverTimeModel({
    this.date,
    this.period,
    this.reason,
    this.images,
    this.status,
    this.note,
    this.createdAt,
  });

  OverTimeModel.fromJson(Map<String, dynamic> json)
      : date = json['date'],
        period = json['period'] == null
            ? null
            : (json['period'] as List).map((e) => e).toList(),
        reason = json['reason'],
        images = json['images'],
        status = json['status'],
        note = json['note'],
        createdAt = json['created_at'];
}

class TimeKeepingTicket {
  final List<String>? timeBlocks;
  final Map<String, dynamic>? statuses;
  final int? status;
  final String? type;
  final num? minute;
  final String? reason;
  final String? createdAt;

  String get txtForType => type == null
      ? ''
      : type!.contains('LATE')
          ? "Xin đi làm muộn"
          : "Xin về sớm";
  TimeKeepingTicket(
      {this.timeBlocks,
      this.statuses,
      this.status,
      this.type,
      this.minute,
      this.reason,
      this.createdAt});
  TimeKeepingTicket.fromJson(Map<String, dynamic> json)
      : timeBlocks = json['time_blocks'] == null
            ? null
            : (json['time_blocks'] as List).map((e) => e.toString()).toList(),
        statuses = json['statuses'],
        status = json['status'],
        type = json['type'],
        minute = json['minute'],
        reason = json['reason'],
        createdAt = json['created_at'];
}
