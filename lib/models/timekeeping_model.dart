import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/models/base_model.dart';

class TimeKeepingModel extends BaseModel {
  final int? id;
  final String? type;
  final String? method;
  final String? status;

  TimeKeepingModel({
    this.id,
    this.type,
    this.method,
    this.status,
  });

  TimeKeepingModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        type = json['type'],
        method = json['method'],
        status = json['status'];

  TimeKeepingModel.withError(ApiError apiErrorDetail)
      : id = 0,
        type = '',
        method = '',
        status = '',
        super.withError(apiErrorDetail);

  Map<String, dynamic> toJson() => {
        'id': id,
        'type': type,
        'method': method,
        'status': status,
      };
}
