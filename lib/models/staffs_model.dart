import 'package:thinkgroup_timekeeping_app/api/api_error.dart';
import 'package:thinkgroup_timekeeping_app/build_config.dart';
import 'package:thinkgroup_timekeeping_app/models/base_model.dart';

class StaffsModel extends BaseModel {
  final int? id;
  final String? code;
  final String? name;
  final String? avatar;
  final String? department;
  final TypeCheck? checkIn;
  final TypeCheck? checkOut;

  StaffsModel({
    this.id,
    this.code,
    this.name,
    this.avatar,
    this.checkIn,
    this.checkOut,
    this.department,
  });

  String get urlAvatar =>
      avatar == null ? '' : BuildConfig.baseMedia + "/" + avatar!;

  StaffsModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        code = json['code'],
        name = json['name'],
        avatar = json['avatar'],
        department = json['department'] ?? '',
        checkIn = json['checkin'] == null
            ? null
            : TypeCheck.fromJson(json['checkin']),
        checkOut = json['checkout'] == null
            ? null
            : TypeCheck.fromJson(json['checkout']);

  StaffsModel.withError(ApiError errorDetail)
      : id = 0,
        code = '',
        name = '',
        avatar = '',
        department = '',
        checkIn = null,
        checkOut = null,
        super.withError(errorDetail);
}

class TypeCheck {
  final int? id;
  final String? type;
  final String? status;
  final String? method;
  final String? methodLabel;
  final String? realDate;
  final String? stardardTime;
  final String? realTime;
  final ImageModel? image;
  final int? timeDiff;
  final String? note;

  TypeCheck({
    this.id,
    this.type,
    this.status,
    this.method,
    this.methodLabel,
    this.realDate,
    this.stardardTime,
    this.realTime,
    this.image,
    this.timeDiff,
    this.note,
  });

  TypeCheck.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        type = json['type'],
        status = json['status'],
        method = json['method'],
        methodLabel = json['method_label'],
        realDate = json['real_date'],
        stardardTime = json['standard_time'],
        realTime = json['real_time'],
        image =
            json['image'] == null ? null : ImageModel.fromJson(json['image']),
        timeDiff = json['time_diff'],
        note = json['note'];

  Map<String, dynamic> toJson() => {
        "id": id,
        "type": type,
        "status": status,
        "method": method,
        "method_label": methodLabel,
        "real_date": realDate,
        "standard_time": stardardTime,
        "real_time": realTime,
        "image": image,
        "time_diff": timeDiff,
        "note": note,
      };
}

class ImageModel {
  final String? name;
  final String? path;
  final int? size;
  final String? type;

  ImageModel({this.name, this.path, this.size, this.type});

  String get urlImage =>
      path == null ? '' : "https://media-api-staging.thinkpro.vn/" + path!;

  ImageModel.fromJson(Map<String, dynamic> json)
      : name = json['name'] ?? '',
        path = json['path'] ?? '',
        size = json['size'] ?? '',
        type = json['type'] ?? '';
}
